<?php
require_once '../config.php';
require_once '../header.php';

if((empty($_POST))) {
    $id = $_GET['id'];
    $sql = "select * from user where id=?";
    $user = $db->GetRow($sql, array($id));
//    print "<pre>";
//
//    print_r($user);
//
//    print "</pre>";
} else {
    $sql = "update user set name=?, password=?, age=?, address=?, phone=?, email=?, status=? where id=?";
    $params = array(
        $_POST['name'], 
        $_POST['age'], 
        $_POST['address'], 
        $_POST['phone'], 
        $_POST['email'], 
        $_POST['status'], 
        $_GET['id']
    );
    $db->execute($sql, $params);
    if(!$db->ErrorNo()) {
        header('Location: http://localhost/bookonline/master_user.php');
    }
    else {
        echo $db->ErrorMsg();
    }

}


?>
<form id="user-form" class="form-vertical" method="post">
    <div class=""> 
        <label class="" style="font-size:20px; padding:3px 0px 0px 180px; color:#007CFF;">Edit users</label>
    </div>
    <div class="form-group">
        <label  class ="bold-label" for="name">Name</label>
        <div class="controls"><input type="text" name="name" class="form-control with-error" value="<?php echo $user['name']?>" id="name" placeholder="Enter name"> </div>
    </div>
    
    <div class="form-group">
        <label class="bold-label" for="age">Age</label>
        <div class="controls"><input type="text" name="age" class="form-control with-error" value="<?php echo $user['age'] ?>"  id="age" placeholder="Enter age"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="address">Address</label>
        <div class="controls"><input type="text" name="address" class="form-control with-error" value="<?php echo $user['address'] ?>" id="address" placeholder="Enter address"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="phone">Phone</label>
        <div class="controls"><input type="text" name="phone" class="form-control with-error" value="<?php echo $user['phone']?>" id="phone" placeholder="Enter phone"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="email">Email</label>
        <div class="controls"><input type="text" name="email" class="form-control with-error" value="<?php echo $user['email']?>" id="email" placeholder="Enter email"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="status">Status</label>
        <div class="controls"><input type="text" name="status" class="form-control with-error" value="<?php echo $user['status'] ?>" id="status" placeholder="Enter status"></div>
    </div>
    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
</form>
