<?php
require_once '../config.php';
require_once '../header.php';
if((isset($_POST['submit']))) {
    $sql = "insert into user(name, password, age, address, phone, email, status) values(?,?,?,?,?,?,?)";
    $params = array(
        $_POST['name'],
        $_POST['pass'],
        $_POST['age'],
        $_POST['address'],
        $_POST['phone'],
        $_POST['email'],
        $_POST['status'],
    );
    $db->execute($sql, $params);
    header('Location: http://localhost/bookonline/master_user.php');
}

?>
<form id="user-form" class="form-vertical" method="post">
    <div class=""> 
        <label class="" style="font-size:20px; padding:3px 0px 0px 180px; color:#007CFF;">New User Form</label>
    </div>
    <div class="form-group">
        <label  class ="bold-label" for="name">Name</label>
        <div class="controls"><input type="text" name="name" class="form-control with-error" id="name" placeholder="Enter name"> </div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="pass">Password</label>
        <div class="controls"><input type="text" name="pass" class="form-control with-error" id="pass" placeholder="Enter password"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="age">Age</label>
        <div class="controls"><input type="text" name="age" class="form-control with-error" id="age" placeholder="Enter age"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="address">Address</label>
        <div class="controls"><input type="text" name="address" class="form-control with-error" id="address" placeholder="Enter address"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="phone">Phone</label>
        <div class="controls"><input type="text" name="phone" class="form-control with-error" id="phone" placeholder="Enter phone"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="email">Email</label>
        <div class="controls"><input type="text" name="email" class="form-control with-error" id="email" placeholder="Enter email"></div>
    </div>
    <div class="form-group">
        <label class="bold-label" for="status">Status</label>
        <div class="controls"><input type="text" name="status" class="form-control with-error" id="status" placeholder="Enter status"></div>
    </div>
    <button type="submit" id="submit" class="btn btn-primary" name="submit">Submit</button>
</form>


