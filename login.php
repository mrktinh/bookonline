<!DOCTYPE html>

<html>
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="style/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="style/login.css">
        <title>Login</title>
</head>
<body>
        <form class="form-horizontal">
            <div class="form-group"> 
                <label class="col-sm-offset-3 col-sm-6" style="font-size:25px">Please Sign in</label>
            </div>
            <div class="form-group">
<!--                <label for="username" class="col-sm-4 control-label">Username</label>-->
                <div class="col-sm-offset-3 col-sm-6">
                  <input type="text" class="form-control" id="username" placeholder="Username">
                </div>
            </div>
            <div class="form-group">
<!--                <label for="password" class="col-sm-4 control-label">Password</label>-->
                <div class="col-sm-offset-3 col-sm-6">
                  <input type="password" class="form-control" id="password" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                  <button type="submit" class="btn btn-primary">Sign in</button>
                </div>
            </div>
        </form>
</body>
</html>
