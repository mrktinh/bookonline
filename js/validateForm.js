$(document).ready(function(){

		$('#user-form').validate({
	    rules: {
                name: {
                    required: true,
                    required: true
                },
		  
		pass: {
                    required: true
                },
                
                age: {
                    number: true
                },
                
                phone: {
                    number: true
                },
		  
                email: {
                  required: true,
                  email: true
                },
                
                status: {
                  required: true,
                  maxlength: 1
                },
		  
                agree: "required"
		  
                },
			highlight: function(element) {
				$(element).closest('.control-group').removeClass('success').addClass('error');
			},
			success: function(element) {
				element
				.text('OK!').addClass('valid')
				.closest('.control-group').removeClass('error').addClass('success');
			}
	  });

}); // end document.ready