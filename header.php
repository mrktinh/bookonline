<html>
    <head>
        <title>Master</title>
        <link rel="stylesheet" type="text/css" href="/bookonline/style/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/bookonline/style/master.css">
        <link rel="stylesheet" type="text/css" href="/bookonline/style/validateForm.css">
        <link rel="stylesheet" type="text/css" href="/bookonline/fonts/glyphicons-halflings-regular.woff">
        <script src="/bookonline/js/jquery.min.js"></script>
        <script src="/bookonline/js/bootstrap.min.js"></script>
        
        <script src="/bookonline/js/validateForm.js"></script>
        <script src="/bookonline/assets/js/jquery.validate.js"></script> 
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                    <ul class="nav nav-tabs navbar-left">
                        <li class="first-menu" id="user-menu"><a href="master_user.php">Users</a></li>
                        <li class="first-menu" id="book-menu"><a href="master_book.php">Books</a></li> 
                        <li class="first-menu" id="publisher-menu"><a href="master_publisher.php">Publisher</a></li>
                        <li class="first-menu" id="topic-menu"><a href="master_topic.php">Topic</a></li> 
                    </ul>
                    <ul class="nav nav-tabs navbar-right">
                        <li><a href="#"><span class="glyphicon">Welcome Admin</span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
            </div>
        </nav>
        <div class="content">
        <script src="/bookonline/js/master.js"></script>    