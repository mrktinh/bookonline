  
<?php
require 'config.php';
require 'header.php';
if (!empty($_POST['chkUser'])) {
    foreach ($_POST['chkUser'] as $key => $value) {
        $sql = "delete from user where id= ?";
        $params = array($value);
        $db->execute($sql, $params);
    }
    if (!$db->ErrorNo()) {
        $result = '<div class="alert alert-success">'
                . '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Delete successfully!</strong></div>';
    }
}

$sql = "SELECT * from user WHERE 1 = 1";
$params = array();
// search
if (!empty($_GET['idSearch'])) {
    $sql .= " AND ID = ?";
    $idSearch = addslashes($_GET['idSearch']);
    $params[] = $idSearch;
}
if (!empty($_GET['nameSearch'])) {
    $sql .= " AND name LIKE ? ";
    $nameSearch = addslashes($_GET['nameSearch']);
    $params[] = "%$nameSearch%";
}
if (!empty($_GET['statusSearch'])) {
    $sql .= " AND status = ? ";
    $statusSearch = addslashes($_GET['statusSearch']);
    $params[] = $statusSearch;
}
if (!empty($_GET['emailSearch'])) {
    $sql .= " AND email = ? ";
    $emailSearch = addslashes($_GET['emailSearch']);
    $params[] = $emailSearch;
}
if (!empty($_GET['phoneSearch'])) {
    $sql .= " AND email = ? ";
    $phoneSearch = addslashes($_GET['phoneSearch']);
    $params[] = $phoneSearch;
}
//if(!empty($_GET['pageSize'])) {
//    $sql .= " LIMIT ? ";
//    $limit = addslashes($_GET['pageSize']);
//    $params[] = intval($limit);
//}




$users = $db->getAll($sql, $params);
if (empty($users)) {
    $notification = '<div class="alert alert-warning">'
            . '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Not Found!</strong></div>';
}
// pagination 
// total record
$totalRecord = sizeof($users);
// find limit and current
$limit = !empty($_GET['limit']) ? intval($_GET['limit']) : 10;
$currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
// find total_page and start
$totalPage = ceil($totalRecord / $limit);
// limit current_page when user enter the input on address
if ($currentPage < 1) {
    $currentPage = 1;
} else if ($currentPage > $totalPage) {
    $currentPage = $totalPage;
}
$startItem = ($currentPage - 1) * $limit;

// get result twice
$sql .= " LIMIT ?,?";
$params[] = $startItem;
$params[] = $limit;
$db->debug = true;
$users = $db->getAll($sql, $params);
?>

<form id="frmMain">
    <div class="form-group">
        <?php
        if (isset($notification)) {
            echo $notification;
        }
        ?>    
    </div>
    <nav class="navbar navbar-default" id="search-bar">
        <div class="navbar-form navbar-left">
            <div class="form-group">
                <input type="text" class="form-control" name="idSearch" placeholder="Search by ID">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="nameSearch" placeholder="Search by Name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="statusSearch" placeholder="Search by Status">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="emailSearch" placeholder="Search by Email">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="phoneSearch" placeholder="Search by Phone">
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="btn btn-success action-nav" href="user/add.php">Add</a>
            <button  type="submit" id="main-del" class="btn btn-success action-nav " formmethod="post"  
                     onclick="return confirmDelete()">Delete</button>
            <button type="submit" class="btn btn-success action-nav">Search</button>
        </div>
    </nav>

    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="checkAll" >
                            </label>
                        </div>
                    </th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($users as $user): ?>
                    <tr>
                        <td>
                            <label>
                                <input type="checkbox" class="checkbox-item" name="chkUser[]" value="<?php echo $user['ID'] ?>" >
                            </label>
                        </td>
                        <td><?php echo $user['ID'] ?></td>
                        <td><?php echo $user['name'] ?></td>
                        <td><?php echo $user['age'] ?></td>
                        <td><?php echo $user['address'] ?></td>
                        <td><?php echo $user['phone'] ?></td>
                        <td><?php echo $user['email'] ?></td>
                        <td><?php echo $user['status'] ?></td>
                        <td>
                            <a class="btn btn-success" href="user/edit.php?id=<?php echo $user['ID'] ?>">Edit</a>
                            <button type="button" class="btn btn-success" onclick="return selectIdToDelete(<?php echo $user['ID'] ?>)">Delete</button>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="pull-left">
                <label for="sel1" class="label-footer">Select list: </label>
                <select class="form-group-lg" id="sel1" name="limit" onchange="this.form.submit()">
                    <?php
                    $options = array(10, 20, 50, 100);
                    foreach ($options as $val) {
                        $pageSize = isset($_GET['limit']) ? $_GET['limit'] : DEFAULT_PAGESIZE;
                        $selected = ($pageSize == $val) ? 'selected' : '';
                        echo "<option value='$val' $selected>$val</option>";
                    }
                    ?>
                </select>
            </div>
            <!--                Display pagination-->
            <div class="pull-right">
                <label for="sel2" class="label-footer">Page Number: </label>
                <select class="form-group-lg" id="sel2" name="page" onchange="this.form.submit()">
                    <?php
                    for($i = 1; $i <= $totalPage ; $i++) {
                        $optionPageNumber[] = $i;
                    }
                    foreach ($optionPageNumber as $val) {
                        $selected = ($currentPage == $val) ? 'selected' : '';
                        echo "<option value='$val' $selected>$val</option>";
                    }
                    ?>
                </select>
            </div>   
        </div>

    </nav>
</form>

<script>
    function confirmDelete() {
        return confirm("Are you sure to delete?");
    }
    function selectIdToDelete(id)
    {
        $('input[name="chkUser[]"]').removeAttr('checked');
        $('input[value=' + id + ']').prop('checked', true);
        $('#main-del').click();
    }
    $('#checkAll').click(function () {
        if ($(this).is(':checked')) {
            $('.checkbox-item').prop('checked', true);
        } else {
            $('.checkbox-item').prop('checked', false);
        }
    });

</script>

<?php
require 'footer.php';
