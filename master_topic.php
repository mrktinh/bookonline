<?php 
require 'config.php';
require 'header.php';
?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <button  class="btn btn-success action-nav">Add</button>
        <button  class="btn btn-success action-nav">Delete</button>
        <button  class="btn btn-success action-nav">Search</button>
        <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search by Name">
            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
</nav>

<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>
                <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                </div>
            </th>
            <th>ID</th>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                </div>
            </td>
            <td>1</td>
            <td>History</td>
            <td>
                <button class="btn btn-success">Edit</button>
                <button class="btn btn-success">Delete</button>
            </td>
        </tr>
        <tr>
            <td>
                <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                </div>
            </td>
            <td>2</td>
            <td>Science</td>
            <td>
                <button class="btn btn-success">Edit</button>
                <button class="btn btn-success">Delete</button>
            </td>
        </tr>

    </tbody>
</table>
<?php 
require 'footer.php';