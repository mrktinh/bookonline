<?php 
require 'config.php';
require 'header.php';
?>
<nav class="navbar navbar-default">
    <form class="navbar-form navbar-left">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search by ID">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search by Name">
        </div>
        <div class="form-group">
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span data-bind="label">Search by Publisher</span>&nbsp;<span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Item 1</a></li>
                    <li><a href="#">Another item</a></li>
                    <li><a href="#">This is a longer item that will not fit properly</a></li>
                </ul>
            </div>
        </div>
        <div class="form-group">
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span data-bind="label">Search by Topic</span>&nbsp;<span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Item 1</a></li>
                    <li><a href="#">Another item</a></li>
                    <li><a href="#">This is a longer item that will not fit properly</a></li>
                </ul>
            </div>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search by Author">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search by Price">
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</nav>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <button  class="btn btn-success action-nav">Add</button>
        <button  class="btn btn-success action-nav">Delete</button>
        <button  class="btn btn-success action-nav">Search</button>
    </div>
</nav>

<table class="table table-bordered table-hover">
    <thead>
        <tr>
            
            <th>
                <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                </div>
            </th>
            <th>ID</th>
            <th>Name</th>
            <th>Author</th>
            <th>Publisher</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                </div>
            </td>
            <td>1</td>
            <td>Toan tong hop</td>
            <td>Le Hong Duc</td>
            <td>Nxb Ha Noi</td>
            <td>150000</td>
            <td>20</td>
            <td>
                <button class="btn btn-success">Edit</button>
                <button class="btn btn-success">Delete</button>
            </td>
        </tr>
        <tr>
            <td>
                <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                </div>
            </td>
            <td>2</td>
            <td>Hoa tong hop</td>
            <td>Le Hong Phong</td>
            <td>Nxb HCM</td>
            <td>250000</td>
            <td>40</td>
            <td>
                <button class="btn btn-success">Edit</button>
                <button class="btn btn-success">Delete</button>
            </td>
        </tr>

    </tbody>
</table>
<?php 
require 'footer.php';